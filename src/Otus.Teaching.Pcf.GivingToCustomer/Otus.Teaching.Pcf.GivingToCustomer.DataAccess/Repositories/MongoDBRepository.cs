﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Linq;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoDbRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly MongoDbDataContext _context;
        private readonly IMongoCollection<T> _collection;

        public MongoDbRepository(MongoDbDataContext context)
        {
            _context = context;
            _collection = _context.GetCollection<T>();
        }

        public async Task<IReadOnlyCollection<T>> GetAllAsync()
        {
            var res = await _collection.FindAsync(FilterDefinition<T>.Empty);
            return await res.ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var filter = new BsonDocument("_id", new BsonDocument("$eq", id));

            var res = await _collection.FindAsync(filter);
            return await res.FirstOrDefaultAsync();
        }

        public async Task<IReadOnlyCollection<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            if (!ids.Any())
            {
                return new List<T>();
            }

            var filter = new BsonDocument("$or", new BsonArray(ids.Select(id =>  new BsonDocument("_id", new BsonDocument("$eq", id)))));
            var res = await _collection.FindAsync(filter);
            return await res.ToListAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var res = await _collection.FindAsync(predicate);
            return await res.FirstOrDefaultAsync();
        }

        public async Task<IReadOnlyCollection<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var res = await _collection.FindAsync<T>(predicate);
            return await res.ToListAsync();
        }

        public Task AddAsync(T entity)
        {
            return _collection.InsertOneAsync(entity);
        }

        public Task UpdateAsync(T entity)
        {
            return _collection.ReplaceOneAsync(item => item.Id == entity.Id, entity, new ReplaceOptions { IsUpsert = true });
        }

        public Task DeleteAsync(T entity)
        {
            return _collection.DeleteOneAsync(item => item.Id == entity.Id);
        }
    }
}
