﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer:IDbInitializer
    {
        private readonly MongoDbDataContext _dataContext;

        public MongoDbInitializer(MongoDbDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public void InitializeDb()
        {
            _dataContext.DropCollection<Customer>();
            _dataContext.DropCollection<Preference>();
            _dataContext.DropCollection<PromoCode>();

            _dataContext.Preferences.InsertMany(FakeDataFactory.Preferences);
            _dataContext.Customers.InsertMany(FakeDataFactory.Customers);
        }
    }
}
