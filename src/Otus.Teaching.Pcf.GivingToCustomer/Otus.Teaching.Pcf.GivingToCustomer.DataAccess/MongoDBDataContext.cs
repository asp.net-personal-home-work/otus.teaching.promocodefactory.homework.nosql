﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class MongoDbDataContext
    {
        private readonly IMongoDatabase _database;

        public MongoDbDataContext(IMongoDatabase database)
        {
            _database = database;
        }

        public IMongoCollection<Customer> Customers => GetCollection<Customer>();

        public IMongoCollection<Preference> Preferences => GetCollection<Preference>();

        public IMongoCollection<PromoCode> PromoCodes => GetCollection<PromoCode>();

        
        public IMongoCollection<TDocument> GetCollection<TDocument>(string collectionName)
        {
            if (string.IsNullOrWhiteSpace(collectionName))
            {
                throw new ArgumentException("Не задано имя коллекции");
            }

            return _database.GetCollection<TDocument>(collectionName);
        }

        public IMongoCollection<TDocument> GetCollection<TDocument>()
        {
            return _database.GetCollection<TDocument>(typeof(TDocument).Name);
        }

        public void DropCollection<TDocument>()
        {
            _database.DropCollection(typeof(TDocument).Name);
        }
    }
}
