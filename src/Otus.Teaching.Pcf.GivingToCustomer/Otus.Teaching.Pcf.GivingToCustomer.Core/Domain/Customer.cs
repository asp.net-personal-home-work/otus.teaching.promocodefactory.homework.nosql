﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Customer
        :BaseEntity
    {
        public Customer()
        {
            Preferences = new List<Guid>();
            PromoCodes = new List<Guid>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public virtual ICollection<Guid> Preferences { get; set; }
        
        public virtual ICollection<Guid> PromoCodes { get; set; }
    }
}